$(document).ready(function(){        
	$('.toggle').click(function(){
        $('.toggle').toggleClass('active')
        $('body').toggleClass('night')
       })
    })

$(function() {
	$(".accordion > .accordion-item").click(function() {
		// Cancel the siblings
		$(this).siblings(".accordion-item").removeClass("is-active").children(".accordion-panel").slideUp();
		// Toggle the item
		$(this).toggleClass("is-active").children(".accordion-panel").slideToggle("ease-out");
	});
});
