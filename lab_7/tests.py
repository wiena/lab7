from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import homepage

# Create your tests here.
class lab7UnitTest(TestCase):
    def test_lab7_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_lab7_urls_is_not_exist(self):
        response = self.client.get('/')
        self.assertFalse(response.status_code==404)

    def test_lab7_using_landingpage_func(self):
        found = resolve('/')
        self.assertEqual(found.func, homepage)

    def test_lab7_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'homepage.html')

    def test_lab7_text_is_exist(self):
        response = Client().get('/')
        self.assertContains(response, "Halo Namaku Tongpooow")
