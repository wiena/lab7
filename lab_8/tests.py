from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.http import HttpRequest
from .views import book
# Create your tests here
class lab8UnitTest(TestCase):
	def test_lab8_url_is_exist(self):
		response = Client().get('/book/')
		self.assertEqual(response.status_code, 200)

	def test_lab8_urls_is_not_exist(self):
		response = self.client.get('/book/')
		self.assertFalse(response.status_code==404)

	def test_lab8_using_landingpage_func(self):
		found = resolve('/book/')
		self.assertEqual(found.func, book)

	def test_lab8_using_template(self):
		response = Client().get('/book/')
		self.assertTemplateUsed(response, 'book.html')

	def test_lab8_text_is_exist(self):
		response = Client().get('/book/')
		self.assertContains(response, "Book")