from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.contrib.auth import authenticate
import requests
import json

# Create your views here.
def book(request):
    return render(request,'book.html')