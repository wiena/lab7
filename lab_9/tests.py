from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.http import HttpRequest
# Create your tests here
class lab9UnitTest(TestCase):
	def test_lab9_url_login_is_exist(self):
		response = Client().get('/accounts/login/')
		self.assertEqual(response.status_code, 200)

	def test_lab9_urls_is_not_exist(self):
		response = self.client.get('/accounts/login')
		self.assertFalse(response.status_code==404)

	def test_lab9_login_user_exists(self):
		response = self.client.get('/accounts/login/')

		self.assertContains(response, 'Username')
		self.assertContains(response, 'Password')

	def test_lab9_register_user_exists(self):
		response = self.client.get('/accounts/register/')

		self.assertContains(response, 'Username')
		self.assertContains(response, 'Password')
		self.assertContains(response, 'Password confirmation')

	def test_register_user(self):
		response = self.client.post('/user/register/', follow=True, data={
			'username': 'wiena123',
			'password1': 'akuseorangkapiten',
			'password2': 'akuseorangkapiten',
        })

